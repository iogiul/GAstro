################################################
#
#
#
# Utils to plot stuff
#
#
#
#
################################################
from __future__ import division
from scipy import linalg
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from astropy.coordinates import SkyCoord
from astropy import units as u
from matplotlib.colors import LogNorm, PowerNorm
from scipy.stats import binned_statistic_2d as bd2
from mpl_toolkits.axes_grid1 import make_axes_locatable
import scipy.ndimage as ndimage


def plot_ellipse(mean,covar,ax=None):

	v, w = linalg.eigh(covar)
	v = 2*np.sqrt(2)*np.sqrt(v)
	u = w[0] / linalg.norm(w[0])
	angle=np.arctan(u[1]/u[0])*(180./np.pi)
	ell = mpl.patches.Ellipse(mean, v[0], v[1], 180+angle)

def ploth1(x=[],ax=None,bins=100,weights=None,linex=[],liney=[],xlim=None,ylim=None,xlabel=None,ylabel=None,fontsize=14,cmap='gray_r',invertx=False,inverty=False,title=None,norm=None,range=None,label='',mode='curve',cumulative=False):

	bins=bins



	if range is None:
		extent=[[np.nanmin(x),np.nanmax(x)]]
	else:
		range=range



	if weights is None: weights=np.ones_like(x)



	H,edges=np.histogram(x,bins,weights=weights,range=range)



	if norm is not None:
		if norm=='max': H=H/np.max(H)
		elif norm=='tot': H=H/np.sum(H)
		else: pass

	if ax is not None:

		if cumulative:
			xplot=np.sort(x)
			yplot=np.arange(len(xplot))/len(xplot)
			im=ax.plot(xplot,yplot,label=label)
		else:
			if mode=='curve':
				xplot=0.5*(edges[:-1]+edges[1:])
				im=ax.plot(xplot,H,label=label)
			elif mode=='step':
				im=ax.step(edges[:-1],H,label=label)

		if len(linex)>0:
			for c in linex:
				ax.plot([c,c],[yedges[0],yedges[-1]],c='blue',lw=2,zorder=1000)

		if len(liney)>0:
			for c in liney:
				ax.plot([xedges[0],xedges[-1]],[c,c],c='blue',lw=2,zorder=1000)

		if xlim is not None: ax.set_xlim(xlim)
		if ylim is not None: ax.set_ylim(ylim)

		if invertx: ax.invert_xaxis()
		if inverty: ax.invert_yaxis()

		if xlabel is not None: ax.set_xlabel(xlabel,fontsize=fontsize,labelpad=2)
		if ylabel is not None: ax.set_ylabel(ylabel,fontsize=fontsize,labelpad=2)
		if title is not None: ax.set_title(str(title),fontsize=fontsize)
	else:
		im=None

	return H,edges,im

def ploth2(x=[],y=[],z=None, statistic='mean', H=None,edges=None,ax=None,
bins=100,
weights=None,
colorbar=True, colorbar_label='', colorbar_axis=None,
linex=[],liney=[],func=[],xlim=None,ylim=None,xlabel=None,ylabel=None,fontsize=14,cmap='gray_r',gamma=1,invertx=False,inverty=False,
interpolation='none',title=None,norm=None,
range=None,
vminmax_option='percentile',vmin=None,vmax=None,
zero_as_blank=True,
levels=None,levels_as_quantile=False,levels_color=None,levels_label=False,levels_fontsize=None,
xlogbin=False, ylogbin=False,
aspect='auto',
smooth=None, sigma_smooth=1):
	'''
	Ploth2 is a function that simplify the making and plotting of 2D histograms  wrapping
	the numpy function np.histrogramdd, the scipy function binned_statistic_2d and
	matplotlib methods imshow, contours and colorbar.
	It accepts as input,
		x,y -> Create (and show)  a classical 2D histogram showing the number of objects in each bin
		x,y,z -> Create (and show) a 2D histrogram in x,y showing a statistic obtained using the values on z
				(the statistic is chosen using the parameter statistic)
		H -> Show the 2D histrogram ssaved in the 2D numpy array H

	If the paramter ax is None, the function will just create the H histogram without creating the plot

	:param x: 1D array containing the value of on the x axis
	:param y: 1D array containing the value of on the y axis
	:param z: 1D array containing the value corresponding to x,y that will be used to estimate the statistic to show
	:param statistic: Only available if z is not None. Statistic to be used to evaluate the value of the 2D histrogram
					in each bin considering the z values collected in that bin.
					It could be a string: 'mean', 'median', 'max', 'min', 'count'  or a function with one parameter.
					Notice that using static='count' is equivalent to not include z since it estimates just the number
					of object in each bin.
	:param H: If provided the values on x,y,z will not be used. The function will just create an image using H as the
			  2D histrograms. If H is not None, the edges should be given too (see below)
	:param edges: bin edges used to create the H histrogram
	:param ax: If None, the function just estimate the H histogram without producing a plot. Otherwise it must be
			  a matplotib axis object
	:param bins: Number of bins to be used. If it is a single number, both axis will use the same number of bins.
				Otherwise if it is a container the X-bin will be equal to bin[0] and Y-bin to bin[1]
	:param weights: Only available if H is None. These are the weights that will be used to produce the 2D histrograms
					They are equivalent to the weights parameter of the numpy function histrogramdd
	:param colorbar: If True add a colorbar to the plot. Only available if ax is not None
	:param colorbar_label: A string to use as colobar axis name
	:param colorbar_axis: If None, the function will automatically set a colorbar close to the axis. Otherwise it must be
			  a matplotib axis object that will containg the colorbar
	:param linex: A float or a  1D container containing float numbers. The plot with show all the vertical lines corresponding to
				these x-values. Only available if ax is not None
	:param liney: A float or a  1D container containing float numbers. The plot with show all the horizontal lines corresponding to
				these y-values. Only available if ax is not None
	:param func: A function or a list of functions of 1 parameter to be showed. The values of the x axis will be used as parameter
				of the functions and their returns will be the y-values.Only available if ax is not None
	:param xlim: limits of the x-axis for the plot. If None they will be the minimum and the maximum of the x-edges. Only available if ax is not None.
	:param ylim: limits of the y-axis for the plot. If None they will be the minimum and the maximum of the y-edges. Only available if ax is not None.
	:param xlabel: xlabel for the x-axis for the plot. Only available if ax is not None.
	:param ylabel: ylabel for the y-axis for the pòot. Only available if ax is not None.
	:param fontsize: fontsize to be used both for the labels and the ticks in the plot. Only available if ax is not None.
	:param cmap: Name of the colormap  to be used to show the histrogram (https://matplotlib.org/stable/tutorials/colors/colormaps.html). Only available if ax is not None.
	:param gamma: Tune the colormap scale. 0 is equivalent to a Log-scaled color map, 1 to a linear colormap. All the other numbers
				create a Power colormap proportional to Value^gamma, where Value are the values of the histrogram.  Only available if ax is not None.
	:param invertx: Invert the order of the x-axis. Only available if ax is not None.
	:param inverty: Invert the order of the y-axis. Only available if ax is not None.
	:param interpolation: The interpolation method used to make the plot. Equivalent to the parameter interpolation
						  of the function imshow of matplotlib. Only available if ax is not None.
	:param title: Show this as axis title. Only available if ax is not None.
	:param norm: if not None, this option set the normalisation of the histogram (if None the histogram is not normalised).
				Otherwise it can be:
				- max: the histogram values are normalised over the maximum value of the histogram
				- tot: the histogram  values are normalised over the sum of all the values in the histogram
				- maxrows: each histogram value is normalised over the maximum value of the row it belongs
				- totrows: each histogram value is normalised over  the sum of all the values in the row it belongs
				- maxcols: each histogram value is normalised over the maximum value of the column it belongs
				- totcols: each histogram value is normalised over  the sum of all the values in the column it belongs
				- percentileXX: the histogram values are normalised over the percentile XX [0-100] of the histogram
								(for example with  percentile50 the histrogram is normalised over the median)
				- density: each value of the histogram is divided by the area of the bin
				- pdf: each value of the histogram is divided by the area of the bin and then it is further divided by the sum of all the values in the histrogram
	:param range: 2D container of the type [[xmin,xmax],[ymin,ymax]]. Only the values withis these x and y ranges will be
				  considered when producing the histogram. If None the whole data range will be used.
	:param vminmax_option: select the way vmin and vmax should be interpreted, it can be:
			- percentile: use the percentiles from 0-100 to set the min and max value of the colormap. [Default]
			- absolte: use the absolute value to set the min and max value of the colormap.
	:param vmin: Maximum value for the scale of the colormap. It could be absolute or relative depending on the vminmax_option.
				 If is None:
				 	- it is set to 5(%) if vminmax_option=percentile
				 	- it is set to the minimum value of the histogram if vminmax_option=absolute
				 Only available if ax is not None.
	:param vmax: Maximum value for the scale of the colormap. It could be absolute or relative depending on the vminmax_option.  Only available if ax is not None.
				 If is None:
				 	- it is set to 95(%) if vminmax_option=percentile
				 	- it is set to the maximum value of the histogram if vminmax_option=absolute
				 Only available if ax is not None.
	:param zero_as_blank: if True set to nan all the points of the histograms storing 0
	:param levels: If not None, it must contain an interger or a container of floats.
					The integer is the number of contour levels that are plotted.
					The floats contains the value of the levels that must be plotted, depending on the levels_as_quantile value they  represent:
						- absolute values (levels_as_quantile=False)
						- quantile values between 0-1 (levels_as_quantile=True). For examples levels=(0.1,0.5,0.7) will show
						the levels that contain the 10%, the 50% and 70% of all the values in the histogram.
					Only available if ax is not None.
	:param levels_as_quantile: If True, interpret the values in levels as quantile otherwise as absolute values. Only available if ax is not None and levels is a container.
	:param levels_color: Color of the levels line. If None it will use the matplotlib default (usually it will use
		a colomap depending onthe value of the levels). Only available if ax is not None and levels is not None.
	:param levels_label:    If True plot the value of the levels on the plot. Only available if ax is not None and levels is not None.
	:param levels_fontsize: Fontsize of the levels label. Only available if ax is not None,  levels is not None and levels_label is True.
	:param xlogbin: If True, produce logarithm bins in the x-axis,
					If True: The bins edges will be estimated from the ranges of the x axis using the function np.logspace
					If False: The bins edges will be estimated from the ranges of the x axis using the function np.linspace
					If True the scale of the x-axis will be set to log
	:param ylogbin: If True, produce logarithm bins in the y-axis
					If True: The bins edges will be estimated from the ranges of the y axis using the function np.logspace
					If False: The bins edges will be estimated from the ranges of the y axis using the function np.linspace
	:param aspect: aspect-ratio for the image. It can be:
					- auto: it will just use the axes-ratio set for the axis  [default]
					- equal: the axis-ratio depends on the ratio between the x-range and y-range
					Only available if ax is not None.
	:param smooth:  If not None, it will use a Gaussian filter to smooth the image using the method ndimage.gaussian_filter from scipy.
					If not None, it can be:
						- both: both the image and the level  contours (if present) will be smoothed.
						- ctr:  only the level  contours (if present) will be smoothed.
					The smooth window is set with the parameter sigma_smooth.
					Only available if ax is not None.
	:param sigma_smooth: Sigma of the smoothing Gaussians in units of bins.
						By default it is 1 (Gaussian sigma=1 bin).
						Only available if ax is not None and smooth is not None.
	:return:
		- H: 2D histogram created by the function
		- edges: tuple of the edges of the 2D histograms created by the functions:
				-edges[0], edges of the x-axis
				-edges[1], edges of the y-axis
		- im: return of the function plt.imshow. It is None if ax is None.
		- cax: axis used to plot the colorbar. It is None if colorbar=False.
	'''
	if H is None:

		if range is None: range = [[np.nanmin(x), np.nanmax(x)], [np.nanmin(y), np.nanmax(y)]]
		else: range = range

		if isinstance(bins,float) or isinstance(bins,int):
			bins_t=[bins,bins]
			samebin=True
		else:
			bins_t=[bins[0],bins[1]]
			samebin=False

		bins=[[0,],[0,]]



		if xlogbin:
			bins[0]=np.logspace(np.log10(range[0][0]),np.log10(range[0][1]),bins_t[0]+1)
			if ax is not None: ax.set_xscale("log")
		else:
			bins[0]=np.linspace(range[0][0], range[0][1],bins_t[0]+1)

		if ylogbin:
			bins[1]=np.logspace(np.log10(range[1][0]),np.log10(range[1][1]),bins_t[1]+1)
			if ax is not None: ax.set_yscale("log")
		else:
			bins[1]=np.linspace(range[1][0], range[1][1],bins_t[1]+1)




		if z is None:
			sample=np.vstack([x,y]).T
			H,edges=np.histogramdd(sample,bins,weights=weights,range=range)
			xedges,yedges=edges
			extent=[xedges[0],xedges[-1],yedges[0],yedges[-1]]


		elif len(z)==len(x):
			H, xedges, yedges,_=bd2(x, y, z, statistic=statistic, bins=bins, range=range, expand_binnumbers=False)
			edges=[xedges, yedges]
			extent=[xedges[0],xedges[-1],yedges[0],yedges[-1]]

		else:
			raise ValueError('Z needs to be None or an array with the same length of z and y')
	else:
		H=H
		xedges,yedges=edges
		extent=[xedges[0],xedges[-1],yedges[0],yedges[-1]]

	if smooth is not None:
		if smooth=="img" or smooth=="both":
			H = ndimage.gaussian_filter(H, sigma=sigma_smooth, order=0)
		elif  smooth=="ctr":
			pass
		else:
			raise ValueError(f"smooth can be None, img, both or ctr not {smooth}")

	if norm is not None:
		if norm=='max':
			H=H/np.nanmax(H)
		elif norm=='tot':
			H=H/np.nansum(H)
		elif norm=='maxrows':
			Hm=np.nanmax(H,axis=0)
			H=H/Hm
		elif norm=='totrows':
			Hm=np.nansum(H,axis=0)
			H=H/Hm
		elif norm=='maxcols':
			Hm=np.nanmax(H,axis=1)
			H=(H.T/Hm).T
		elif norm=='totcols':
			Hm=np.nansum(H,axis=1)
			H=(H.T/Hm).T
		elif norm[:10]=='percentile':
			q=float(norm[10:12])
			Hm=np.nanpercentile(H,q=q)
			H=H/Hm
		elif norm=="density":
			dx = np.diff(edges[0])
			dy = np.diff(edges[1])
			areabin=dx[:,  None] * dy
			H=H/(areabin)
		elif norm=="pdf":
			dx = np.diff(edges[0])
			dy = np.diff(edges[1])
			areabin=dx[:,  None] * dy
			H=H/(np.nansum(H)*areabin)
		else: raise ValueError('norm option %s not recognised (valide values: max, tot, maxcols, maxrows, totcols, totrows, density,pdf)'%str(norm))

	if zero_as_blank:
		H=np.where(H==0,np.nan,H)

	if ax is not None:

		if vminmax_option=='percentile':
			if vmax is None: vmaxM=np.nanpercentile(H,q=95)
			else: vmaxM=np.nanpercentile(H,q=vmax)
			if vmin is None: vminM=np.nanpercentile(H,q=5)
			else: vminM=np.nanpercentile(H,q=vmin)
		elif vminmax_option=='absolute':
			if vmax is None: vmaxM=np.nanmax(H)
			else: vmaxM=vmax
			if vmin is None: vminM=np.nanmin(H)
			else: vminM=vmin
		else: raise ValueError("Unknown vminmax option "+str(vminmax_option)+". Please chose percentile or absolute")
		#X,Y=np.meshgrid(xedges,yedges)


		if xlogbin or ylogbin:

			xim = edges[0]
			yim = edges[1]

			if gamma==0: im=ax.pcolor(xim,yim,H.T,cmap=cmap,norm=LogNorm(vmax=vmaxM,vmin=vminM))
			else: im=ax.pcolor(xim,yim,H.T,cmap=cmap,norm=PowerNorm(gamma=gamma,vmax=vmaxM,vmin=vminM))

		else:
			if gamma==0: im=ax.imshow(H.T,origin='lower',extent=extent, aspect=aspect,cmap=cmap,norm=LogNorm(vmax=vmaxM,vmin=vminM),interpolation=interpolation)
			else: im=ax.imshow(H.T,origin= 'lower',extent=extent, aspect=aspect,cmap=cmap,norm=PowerNorm(gamma=gamma,vmax=vmaxM,vmin=vminM),interpolation=interpolation)

		ax.tick_params(labelsize=fontsize)


		linex=np.atleast_1d(linex)
		if len(linex)>0:
			for c in linex:
				ax.plot([c,c],[yedges[0],yedges[-1]],c='blue',lw=2,zorder=1000)

		liney=np.atleast_1d(liney)
		if len(liney)>0:
			for c in liney:
				ax.plot([xedges[0],xedges[-1]],[c,c],c='blue',lw=2,zorder=1000)

		func=np.atleast_1d(func)
		if len(func)>0:
			if xlim is not None: xf=np.linspace(xlim[0],xlim[1])
			else: xf=np.linspace(range[0][0],range[0][1])
			for f in func:
				ax.plot(xf,f(xf),c='blue',lw=2,zorder=1000)

		if levels is not None:

			if smooth is not None and (smooth=="ctr" or smooth=="both"):
				Hc = ndimage.gaussian_filter(H, sigma=sigma_smooth, order=0)
			else:
				Hc=H

			if levels_as_quantile and np.all(np.array(levels)<1):
				Hcf=np.sort(Hc.flatten()) #Flatten the array
				#Cumulative distribution of the flattened array
				sm=np.nancumsum(Hcf)
				print(sm)
				sm/=sm[-1]
				#Now for each quantile find the equivalent level
				levels=[Hcf[sm<=lev][-1] for lev in levels]

			elif levels_as_quantile:
				raise ValueError(f"levels_as_quantile={levels_as_quantile} but some of  level values are >1: {levels} ")

			if levels_color is None:
				CS=ax.contour(Hc.T,origin='lower',extent=extent,levels=levels)
			else:
				CS=ax.contour(Hc.T, origin='lower', extent=extent, levels=levels,colors=levels_color)
			if levels_label:
				if levels_fontsize is None: levels_fontsize=fontsize
				ax.clabel(CS, CS.levels, inline=True, fontsize=levels_fontsize)

		if xlim is not None: ax.set_xlim(xlim)
		if ylim is not None: ax.set_ylim(ylim)

		if invertx: ax.invert_xaxis()
		if inverty: ax.invert_yaxis()

		if xlabel is not None: ax.set_xlabel(xlabel,fontsize=fontsize,labelpad=2)
		if ylabel is not None: ax.set_ylabel(ylabel,fontsize=fontsize,labelpad=2)
		if title is not None: ax.set_title(str(title),fontsize=fontsize)
	else:
		im=None



	if colorbar and (ax is not None):
		divider = make_axes_locatable(ax)
		if colorbar_axis is not None: cax=colorbar_axis
		else: cax     = divider.append_axes('right', size='5%', pad=0.05)

		cbar = plt.colorbar(im, cax=cax)
		cbar.ax.set_ylabel(colorbar_label, fontsize=fontsize)
		cbar.ax.tick_params(labelsize=fontsize)
	else: cax=None

	edges=(xedges,yedges)

	#if ax is not None: plt.sca(ax)
	return H,edges,im,cax

def make_colorbar(im, ax, label=None, position='right',size='5%', pad=0.05, fontsize=None):

	divider = make_axes_locatable(ax)
	cax = divider.append_axes(position, size=size, pad=pad)
	cbar = plt.colorbar(im, cax=cax)
	if fontsize is None: fontsize=mpl.rcParams['font.size']
	cbar.ax.set_ylabel(label, fontsize=fontsize)

	return cbar, cax
